In this mode every file located in the root src directory should contain a unique code representing the course to which the file belongs to. 

File names should follow this syntax:
`<fileName>.<courseCode>. <fileExtension>`