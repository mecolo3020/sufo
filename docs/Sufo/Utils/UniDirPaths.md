This Class implements and manages University Directories. It has the next attributes and methods:

### Attributes:
- *srcDirToOrganizePath*, source directory path that needs organizaiton
- *dstRootUniDirPath*, destination root directory path. This path should be your directory containing all your university notes.
- *dstJsonDirStructPath*, json file used to describe the internal structure that dstRootUniDirPath should have.
- orgModeChoice, an enum that specifies the organizaiton mode.
### Methods:
- 