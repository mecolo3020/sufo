Simple University File Organizer. Made in python for my girlfriend; she is such a mess with her 🖥. 

This project has a [[CLI]] and a [[GUI]]. The first is used for testing and development purposes. The normal way to use this tool should be by running the [[GUI]]. 

[[CORE]] is the python module that contains all the functionality needed to organize the files. 