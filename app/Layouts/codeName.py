def GetCodeNameLayout(gui, courses={999: "whatafac"}, courseInputTextKey="-IN-"):
    InputTextKey = courseInputTextKey

    courses = [
        [
            gui.Text("\t📚 " + courseName + ":"),
            gui.InputText(
                courseCode,
                use_readonly_for_disable=True,
                disabled=True,
                key=InputTextKey.format(courseCode=courseCode),
            ),
        ]
        for courseCode, courseName in courses.items()
    ]

    CodeNameLayout = [
        [gui.Button("ORGINIZE THIS SHIT FOR ME!")],
        [gui.Text("List of code names:")],
    ]

    for course in courses:
        CodeNameLayout.append(course)

    return CodeNameLayout
