def GetButtonsLayout(
    gui,
    courses=["whatafac"],
    courseButtonKeys="-BUTTON-",
    currentSelectedDestination=""
):
    Button_size = (11, 1)
    count = 0
    for course in courses:
        if count % 6 == 0:
            print(course)
            count += 1
    # TODO generalizzare questo layout
    ButtonsLayout = [
        [
            gui.Text("Select the direcotry that contains files to organize:"),
            gui.In(size=(25, 1), enable_events=True, key="-CWD-"),
            gui.FolderBrowse(),  # di default fa riferimento all'elemento subito prima di lui
        ],
        [
            gui.Text("Choose a destination for all files:"),
            gui.Text(currentSelectedDestination),
        ],
        [
            gui.Button("Programing 1", size=Button_size),
            gui.Button("Programing 2", size=Button_size),
            gui.Button("Linear Algebra", size=Button_size),
            gui.Button("AI principles", size=Button_size),
            gui.Button("Nome test", size=Button_size),
            gui.Button("test", size=Button_size),
        ],
        [
            gui.Button("test", size=Button_size),
            gui.Button("test", size=Button_size),
            gui.Button("test", size=Button_size),
            gui.Button("test", size=Button_size),
        ],
        [
            gui.Button("test 2", size=Button_size),
            gui.Button("test 2", size=Button_size),
            gui.Button("test 2", size=Button_size),
            gui.Button("test 2", size=Button_size),
            gui.Button("test 2", size=Button_size),
        ],
        [
            gui.Button("test 3", size=Button_size),
            gui.Button("test 3", size=Button_size),
            gui.Button("test 3", size=Button_size),
            gui.Button("test 3", size=Button_size),
            gui.Button("test 3", size=Button_size),
        ],
    ]
    return ButtonsLayout
