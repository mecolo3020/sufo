from enum import IntEnum, unique


@unique
class OrganizationModes(IntEnum):
    BY_COURSECODE = 0  # diferent files need to go to different directories
    BY_CATEGORY = 1  # a bunch of files need to go to the same direcotry

    # overwriten
    def __repr__(self):
        return "%s: %r" % (self._name_, self._value_)
