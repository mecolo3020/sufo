import os
import json
from .CustomFunctions import isEmptyList
from .CustomEnums import OrganizationModes


class UniversityDirectory:
    """
    this class implements and manages Univeristy Directories.
    """

    def __init__(
        self,
        srcDirToOrganizePath: str = "",
        dstRootUniDirPath: str = "",
        dstJsonDirStructTemplatePath: str = "",
        orgModeChoice: int = OrganizationModes.BY_COURSECODE,
    ) -> None:
        self._srcDirToOrganizePath = srcDirToOrganizePath
        self._dstRootUniDirPath = dstRootUniDirPath
        self._dstJsonDirStructTemplatePath = dstJsonDirStructTemplatePath
        self._orgModeChoice = orgModeChoice

    def get_srcDirToOrganizePath(self) -> str:
        return self._srcDirToOrganizePath

    def get_dstRootUniDirPath(self) -> str:
        return self._dstRootUniDirPath

    def get_dstJsonDirStructTemplatePath(self) -> str:
        return self._dstJsonDirStructTemplatePath

    def get_orgModeChoice(self) -> int:
        return self._orgModeChoice

    def set_srcDirToOrganizePath(self, srcDirToOrganizePath: str = "") -> None:
        self._srcDirToOrganizePath = srcDirToOrganizePath

    def set_dstRootUniDirPath(self, dstRootUniDirPath: str = "") -> None:
        self._dstRootUniDirPath = dstRootUniDirPath

    def set_dstJsonDirStructPath(self, dstJsonDirStructPath: str = "") -> None:
        self._dstJsonDirStructPath = dstJsonDirStructPath

    def set_orgModeChoice(self, orgModeChoice: int) -> None:
        self._orgModeChoice = orgModeChoice

    def createFileStructureFromJson(self) -> bool:
        """
        Given a root directory and a direcotry structure, this functions checks the directory
        structure wanted based on root directory; if any direcotry is missing it creates it based on jsonDirStructure.

        🔙 return: True if creation was succesful, False otherwise.
        """
        if (
            self.get_dstRootUniDirPath() == ""
            or self.get_dstJsonDirStructTemplatePath() == ""
        ):
            return False

        dirStructure = open(self.get_dstJsonDirStructTemplatePath(), mode="r")
        rootUniDirStruct = json.loads(dirStructure.read())

        os.chdir(self.get_srcDirToOrganizePath())

        # TODO walk Root Uni Directory structure, if somewhere there is a missing direcotry create it recursisvely
        for dirPath, dirNames, fileNames in os.walk(self.get_dstRootUniDirPath()):
            # TODO identificare che mi trovo in una foglia, tipo Programming1
            # se in dirNames e' vuota o siamo in una foglia o non esiste la struttura di sottocartelle

            # TODO iterare contemporaneamente rooUniDirStruct e os.walk(self.get_dstRootUniDirPath())
            print("dirPath", dirPath)
            # if isEmptyList(dirNames) and isEmptyList(fileNames):
            #     print("FOGLIA: ", dirPath)

            # else:
            #     print(f"dirPath: {dirPath}")
            #     print(f"dirNames: {dirNames}")
            #     print(f"fileNames: {fileNames}")

        dirStructure.close()
        return True

    @classmethod
    def splitFileName(cls, fileName: str) -> "tuple[str, str, str]":
        """
        ⩥ param fileName: file's name to be splitted.

        Splits fileName in its components:
            * file's name
            * course code ➡ needed to put the file in its respective directory
            * file's extension

        🔙 return: (fileName, courseCode, fileExtension);
        """

        if fileName == "":
            return ("", "", "")

        nameCourseCode, dotExtension = os.path.splitext(fileName)
        name, dotCourseCode = os.path.splitext(nameCourseCode)
        return (name, dotCourseCode.replace(".", ""), dotExtension.replace(".", ""))
