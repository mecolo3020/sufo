def isEmptyList(l: "list[str]" = []) -> bool:
    """
    if list is [ ] returns true otherwise false.
    """
    return not bool(l)

