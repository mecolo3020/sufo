# TODO here i should from CORE import all I need
import os
from Utils.CustomEnums import OrganizationModes
from Utils.Constants import Bcolors
from Utils.UniDirPaths import UniversityDirectory

# this .py file is used as CLI
if __name__ == "__main__":
    # uniDir = UniversityDirectory(
    #     srcDirToOrganizePath=input("directory's path that needs to be organized: "),
    #     dstRootUniDirPath=input("pls insert path to destionation directory: "),
    #     dstJsonDirStructTemplatePath=input("path for json with directory structure: "),
    #     orgModeChoice=int(input(f"choose an organizaiton mode (Options:{list(OrganizationModes)})")),
    # )
    uniDir = UniversityDirectory(
        srcDirToOrganizePath="C:\\Users\\carlo\\Desktop\\PyFileOrganizer\\sufo\\TestRoot\\desktop",
        dstRootUniDirPath="C:\\Users\\carlo\\Desktop\\PyFileOrganizer\\sufo\\TestRoot\\rootUniDir",
        dstJsonDirStructTemplatePath="C:\\Users\\carlo\\Desktop\\PyFileOrganizer\\sufo\\uniDirStructure.json",
        orgModeChoice=0,
    )

    while not (uniDir.get_orgModeChoice() in list(OrganizationModes)):
        print(
            f"\n{Bcolors.FAIL}No Organization mode with value {uniDir.get_orgModeChoice()} exists!{Bcolors.ENDC}"
        )

    while uniDir.get_srcDirToOrganizePath() == "":
        uniDir.set_srcDirToOrganizePath(
            input(
                f"\n{Bcolors.WARNING}Directory's path that needs to be organized was empty, pls insert a path: {Bcolors.ENDC}"
            )
        )
    while uniDir.get_dstRootUniDirPath() == "":
        uniDir.set_dstRootUniDirPath(
            input(
                f"\n{Bcolors.WARNING}Root university directory's path was empty, pls insert a path: {Bcolors.ENDC}"
            )
        )
    while uniDir.get_dstJsonDirStructTemplatePath() == "":
        pathJsonDirStructure = input(
            f"\n{Bcolors.WARNING}Json structure file's path was empty, pls insert a path: {Bcolors.ENDC}"
        )

    uniDir.createFileStructureFromJson()

    os.chdir(uniDir.get_srcDirToOrganizePath())
    if uniDir.get_orgModeChoice() == OrganizationModes.BY_COURSECODE:
        print("files to move: ", os.listdir())
        for fileName in os.listdir():
            _, courseCode, __ = UniversityDirectory.splitFileName(fileName)

    elif uniDir.get_orgModeChoice() == OrganizationModes.BY_CATEGORY:
        print("WIP ⚒")
        pass
