import PySimpleGUI as gui
from Layouts.buttons import GetButtonsLayout as ButtonsLayout
from Layouts.codeName import GetCodeNameLayout as CodeNameLayout
from Utils.CustomEnums import OrganizationModes

# TODO use OrganizationModes to set values in buttons

currentSelectedDestination = ""
WindowName = "PyFileOrganizer"
gui.set_options(font=("Arial", 14, "normal"))
courses = {
    0: "Programming 1",
    1: "Programming 2",
    2: "Linear Algebra",
    3: "AI Principles",
    4: "TEST4",
    5: "TEST5",
    6: "TEST6",
    7: "TEST7",
    8: "TEST8",
    9: "TEST9",
    10: "TEST10",
    11: "TEST11",
}
courseInputTextKeys = "-IN{courseCode}-"
courseButtonKeys = "-BUTTON{courseCode}-"

layout = [
    [
        gui.Column(
            layout=ButtonsLayout(
                gui=gui,
                courses=courses.values()
            ),
            key="-PAGE1-",
        ),
        gui.Column(
            layout=CodeNameLayout(
                gui=gui,
                courses=courses,
                courseInputTextKey=courseInputTextKeys,
            ),
            key="-PAGE2-",
            visible=False,
        ),
    ],
    [gui.Button("Switch orginazing mode", k="-SWITCH_MODE-")],
]

window = gui.Window(WindowName, layout=layout, finalize=True)

# making course codes selectables and visually pleasing
for courseCode in courses.keys():
    window[courseInputTextKeys.format(courseCode=courseCode)].Widget.config(
        readonlybackground=gui.theme_background_color()
    )
    window[courseInputTextKeys.format(courseCode=courseCode)].Widget.config(
        borderwidth=0
    )

PageNum = 1
# event loop
while True:
    event, values = window.read()

    if event in (None, "Exit") or event == gui.WIN_CLOSED:
        break

    if event == "-SWITCH_MODE-":
        window[f"-PAGE{PageNum}-"].update(visible=False)

        PageNum = (PageNum + 1) % 3
        if PageNum == 0:
            PageNum = 1

        window[f"-PAGE{PageNum}-"].update(visible=True)
    # se volessi dare l'opportunita di scegliere la pagina...
    # elif event in "123":
    #     window[f"-COL{PageNum}-"].update(visible=False)
    #     PageNum = int(event)
    #     window[f"-COL{PageNum}-"].update(visible=True)
window.close()
