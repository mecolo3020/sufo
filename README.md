# SUFO (Simple University Files Organizer)

## TODO

- [ ] develop GUI ⚒
- [ ] develop business logic ⚒
- [ ] create a batch to launc the app

## Description

A simple file orginizer for my girlfriend, her desktop tiggers my OCD

## Visuals

![PreAlphaGui](https://i.imgur.com/4t90is5.png)

## Installation

- use the requirements.txt to create a pipenv venv
- launch the batch file to start the app

## Usage

move all files to one destination folder or organize them by a code in their names

## Authors and acknowledgment

Mecolo

## License

i will think about this if this repo goes public

## Project status

WIP
